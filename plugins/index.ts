import '@/assets/scss/common/build_common.scss';
import Vue from "vue";
// @ts-ignore
import VueSlider from 'vue-slider-component/dist-css/vue-slider-component.umd.min.js';
import 'vue-slider-component/dist-css/vue-slider-component.css'
import 'vue-slider-component/theme/default.css'

import VueScrollTo from 'vue-scrollto';

import Typo from '@/plugins/Typo/Typo.vue';
import Button from '@/plugins/Button/Button.vue';
import Image from '@/plugins/Image/Image.vue';
import Icon from '@/plugins/Icon/Icon.vue';
import Headline from '@/plugins/Headline/Headline.vue';

Vue.component('v-typo', Typo);
Vue.component('v-button', Button);
Vue.component('v-image', Image);
Vue.component('v-icon', Icon);
Vue.component('v-headline', Headline);

Vue.component('v-range-slider', VueSlider);
Vue.use(VueScrollTo);

Vue.use(VueScrollTo, {
  container: "body",
  duration: 500,
  easing: "ease",
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true
});
