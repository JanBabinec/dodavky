import {Ball} from '@/plugins/Blob/Ball';

type config = {
	ctx: CanvasRenderingContext2D,
	balls: Array<Ball>,
	plate: HTMLCanvasElement
}

export default class BlobWrapper {
	plates: NodeList | undefined;

	constructor() {

		if (typeof window === 'undefined') {
			return;
		}

		this.plates = document.querySelectorAll('.blob');

		if (!this.plates.length) {
			return;
		}

		this.generateBalls();
		this.render();
	}

	resizePlate(plate: any) {
		let parent = plate.parentNode as HTMLElement;
		plate.width = parent.clientWidth;
		plate.height = parent.clientHeight;

	}

	bindEvents() {
		window.addEventListener('resize', ()=>{
			Array.prototype.forEach.call(this.plates, plate => this.resizePlate(plate))
		})

	}

	generateBalls() {
		Array.prototype.forEach.call(this.plates, (plate) => {

			this.resizePlate(plate);

			const ctx = plate.getContext('2d');

			const config: config = {
				ctx,
				balls: [],
				plate
			};

			const length = 6;

			for (let i = 0; i < length; i++) {
				//@ts-ignore
				config.balls.push(new Ball(
					plate.width / 2 + (i * (Math.random() * 5)) + plate.width / 2 * Math.sin(i * 2 * Math.PI / length),
					plate.height / 2 - (i * (Math.random() * 5)) + plate.height / 2 * Math.cos(i * 2 * Math.PI / length),
					Math.max(Math.random() * 3.5, 0.95),
					2,
					'#ff0000'
					)
				)
			}

			// @ts-ignore
			Ball.instances.push(config);

		});
	}

	connectBalls(config: config) {
		const gradient = config.ctx.createLinearGradient(0, 0, 0, 135);
		gradient.addColorStop(0, '#dee0ea');
		gradient.addColorStop(1, '#bdbfc2');

		config.ctx.beginPath();

		// Start at first ball postion
		config.ctx.moveTo(config.balls[0].x, config.balls[0].y);

		//Connect Balls
		for (let i = 0, jlen = config.balls.length; i <= jlen; ++i) {
			const p0 = config.balls[i >= jlen ? i - jlen : i];
			const p1 = config.balls[i + 1 >= jlen ? i + 1 - jlen : i + 1];
			config.ctx.quadraticCurveTo(p0.x, p0.y, (p0.x + p1.x) * 0.5, (p0.y + p1.y) * 0.5);
		}

		config.ctx.closePath();

		config.ctx.fillStyle = gradient;
		config.ctx.fill();
	}

	render(t: number = 0) {
		Ball.instances.forEach((config: config) => {
			config.ctx.clearRect(0, 0, config.plate.width, config.plate.height);
			config.balls.forEach((ball) => {
				ball.morph(t);
			});

			this.connectBalls(config);
		});

		window.requestAnimationFrame(() => {
			this.render(t + 0.003);
		});
	}

}
