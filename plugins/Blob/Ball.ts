export class Ball {
  x: number;
  y: number;
  originalX: number;
  originalY: number;
  vx: number;
  vy: number;
  radius: number;
  color: string;
  /*friction: number;
  springFactor: number;*/
  weight: number;

  constructor(x: number, y: number, weight: number, radius: number = 2, color: string = '#ff0000') {
    this.x = x || 0;
    this.y = y || 0;

    this.originalX = x || 0;
    this.originalY = y || 0;

    this.vx = 0;
    this.vy = 0;

    this.radius = radius;
    this.color = color;
/*

    this.friction = 0.01;
    this.springFactor = 0.01;
*/

    this.weight = weight || 1;
  }

  morph(t: number) {
    this.vx = this.weight / 24 * Math.cos(this.radius * t * this.weight);
    this.vy = this.weight / 24 * Math.sin(this.radius * t * this.weight);

    this.x += this.vx;
    this.y += this.vy;
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.save();
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
    ctx.fillStyle = this.color;
    ctx.fill();
    ctx.closePath();
    ctx.restore();
  }

  static instances = [];

}
