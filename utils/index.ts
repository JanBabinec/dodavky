import debounce from '@/utils/debounce';
import {getResolution, isRetina} from '@/utils/display';

export {
	debounce,
	isRetina,
	getResolution
};
