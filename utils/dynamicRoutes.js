const axiosInstance = require('./axiosInstance');
const slugify = require('slugify');

async function DynamicRoutes() {
	const response = await axiosInstance.get(`${process.env.API_URL}category/`);

	return response.data.map((category) => {
		return {
			route: `/${slugify(category.title, {lower: true})}`,
			payload: category
		}
	});
}

module.exports = DynamicRoutes;
