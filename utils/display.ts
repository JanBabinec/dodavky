const isRetina = (): boolean => {
	if (typeof window === 'undefined') {
		return false;
	}

	// use devicePixelRation as default if supported by browser
	if (window.devicePixelRatio && window.devicePixelRatio > 1) {
		return true;
	}

	// use matchMediaQuery as fallback
	return window.matchMedia && window.matchMedia(`
			(-webkit-min-device-pixel-ratio: 1.5),
			(min--moz-device-pixel-ratio: 1.5),
			(-o-min-device-pixel-ratio: 3/2),
			(min-resolution: 1.5dppx)
		`).matches;
};

const getResolution = (): any => {
	if (typeof window === 'undefined') {
		return [];
	}

	const resolutions: { tiny: number, medium: number, large: number } = {
		tiny: 0,
		medium: 901,
		large: 1201
	};

	return Object
		.keys(resolutions)
		.filter((key) => (resolutions as any)[key] <= window.innerWidth);
};

export {
	isRetina,
	getResolution
};

export default getResolution;
