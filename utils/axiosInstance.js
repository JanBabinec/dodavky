const axios = require('axios');

const axiosInstance = axios.create({
	baseURL: process.env.API_URL,
	timeout: process.env.API_TIMEOUT || 300000
});

// axiosInstance.defaults.withCredentials = true;

axiosInstance.interceptors.response.use((response) => {
	return response;
}, (error) => {
	// request cancellation
	if(error.__CANCEL__) {
		return false;
	}

	// actual request error
	return Promise.reject(error);
});

module.exports = axiosInstance;
