export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Drive | Půjčovna dodávek Plzeň',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'author', name: 'author', content: 'janbabinec.cz'},
      {property: 'keywords', content: 'Půjčovna, dodávky, Plzeň, auta, Drive, pronájem'},
      {property: 'description', content: 'Půjčovna dodávek a osobních aut v Plzni a okolí'},
      {property: 'og:type', content: 'website'},
      {property: 'og:site_name', content: 'TODO'},
      {property: 'og:image', content: 'https://www.[fillUrl].cz/og.png'},
      {property: 'og:image:url', content: 'https://www.[fillUrl].cz/og.png'},
      {property: 'og:image:secure_url', content: 'https://www.[fillUrl].cz/og.png'},
      {property: 'og:image:width', content: '1200'},
      {property: 'og:image:height', content: '630'},
      {name: 'google-site-verification', content: 'Private CODE'}
    ],
    link: [
      {rel: 'apple-touch-icon', sizes: '57x57', href: '/fav/apple-icon-57x57.png'},
      {rel: 'apple-touch-icon', sizes: '60x60', href: '/fav/apple-icon-60x60.png'},
      {rel: 'apple-touch-icon', sizes: '72x72', href: '/fav/apple-icon-72x72.png'},
      {rel: 'apple-touch-icon', sizes: '76x76', href: '/fav/apple-icon-76x76.png'},
      {rel: 'apple-touch-icon', sizes: '114x114', href: '/fav/apple-icon-114x114.png'},
      {rel: 'apple-touch-icon', sizes: '120x120', href: '/fav/apple-icon-120x120.png'},
      {rel: 'apple-touch-icon', sizes: '144x144', href: '/fav/apple-icon-144x144.png'},
      {rel: 'apple-touch-icon', sizes: '152x152', href: '/fav/apple-icon-152x152.png'},
      {rel: 'apple-touch-icon', sizes: '180x180', href: '/fav/apple-icon-180x180.png'},
      {rel: 'icon', type: 'image/png', href: '/fav/android-icon-192x192.png'},
      {rel: 'icon', sizes: '32x32', href: '/fav/favicon-32x32.png'},
      {rel: 'icon', sizes: '96x96', href: '/fav/favicon-96x96.png'},
      {rel: 'icon', sizes: '16x16', href: '/fav/favicon-16x16.png'},
      {rel: 'icon', type: 'image/x-icon', href: '/fav/favicon.ico'},
      {rel: 'stylesheet', href: 'https://use.typekit.net/adh4ijn.css'}

    ],
    bodyAttrs: {
      class: 'page'
    },
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {color: '#fff'},
  /*
   ** Global CSS
   */

  /*
	** Global styles
	*/
  styleResources: {
    scss: [
      '~assets/scss/core/_core.scss'
    ]
  },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {src: '~/plugins/index.ts'},
    {src: '~/plugins/gmap.ts', ssr: false}
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/style-resources-module
    '@nuxtjs/style-resources',
    // Doc: https://github.com/rigor789/vue-scrollto
    'vue-scrollto/nuxt',
  ],
  /*
   ** Build configuration
   */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      config.output.publicPath = '/_nuxt/';
      [
        {
          test: /\.(png|jpg|gif|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path]/[name].[ext]?[hash]',
                outputPath: ''
              }
            },
            {
              loader: 'image-webpack-loader',
              options: {
                mozjpeg: {
                  quality: 80
                },
                optipng: {
                  enabled: false,
                },
                pngquant: {
                  quality: [0.80, 0.80]
                },
                gifsicle: {
                  interlaced: false,
                }
              }
            }
          ]
        }
      ];
      return config;
    },
    /*
    ** Extract CSS
    */
    extractCSS: false,
    optimization: {
      splitChunks: {
        cacheGroups: {
          styles: {
            name: 'styles',
            test: /\.(css|vue)$/,
            chunks: 'all',
            enforce: true
          }
        }
      }
    },


  }
}
