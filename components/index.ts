import Calculator from '~/components/Calculator.vue';
import CarInfo from '~/components/CarInfo.vue';
import Contact from '~/components/Contact.vue';
import Rules from '~/components/Rules.vue';
import Visual from '@/components/Visual.vue';

export {
  Calculator,
  CarInfo,
  Contact,
  Rules,
  Visual,
}
