<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Read the raw JSON data from the request body
  $data = file_get_contents("php://input");

  // Decode the JSON data into an associative array
  $postData = json_decode($data, true);

  if (!$postData) {
    // JSON decoding failed
    echo "Failed to decode JSON data.";
  } else {
    // Process the data
    $name = $postData["name"];
    $address = $postData["address"];
    $email = $postData["email"];
    $phone = $postData["phone"];
    $birth = $postData["birth"];
    $reservationStart = $postData["reservationStart"];
    $reservationEnd = $postData["reservationEnd"];

    $to = "kamil.javorsky@gmail.com";
    $subject = "New email from $name";
    $body = "Jméno: $name\nTelefon: $phone\nBydliště: $address\nE-mail: $email\nDatum narození: $birth\nZačátek rezervace: $reservationStart\nKonec rezervace: $reservationEnd";

    if (mail($to, $subject, $body)) {
      echo "Děkujeme za zprávu, budeme vás kontaktovat";
    } else {
      echo "Failed to send email. Check your server's mail configuration.";
    }
  }
}
?>
